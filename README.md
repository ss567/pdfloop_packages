* To include a PDFset, make sure the directory containing the PDFset has '${PDFNAME}.info' file.
* Check the SetIndex number in the ${PDFNAME}.info.
* Make sure if the SetIndex is present in the PDFLOOP_PACKAGES/share/LHAPDF/pdfsets.index and matches the ${PDFNAME}
eg: 
```
   #inside PDFLOOP_PACKAGES
   grep 25300 share/LHAPDF/pdfsets.index
   25300 MMHT2014nnlo68cl 3
```

* if the SetIndex does not match ${PDFNAME}, then rename all the files and ${PDFNAME}.info content in the PDFset to match the pdf name in the pdfsets.index for that SetIndex.
* Finally copy the PDFset directory inside PDFLOOP_PACKAGES/share/LHAPDF/
* tarball PDFLOOP_PACKAGES